from flask import Flask, render_template, redirect, url_for, request
import sqlite3,hashlib
import os
from datetime import datetime

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def login():
    error = None
    ip = request.remote_addr
    today = datetime.now()
    day = today.day
    month = today.month
    year = today.year	
    data = str( "data: "+str(day)+"/"+str(month)+"/"+str(year))
    hora = str( " hora: "+str(today.hour)+":"+ str(today.minute)+":"+str(today.second))
    nomearquivoTres = 'visitou'+'.txt'
    if os.path.exists(nomearquivoTres):
       append_write = 'a'
    else:
       append_write = 'w'
    arquivo = open(nomearquivoTres,append_write)
    arquivo.write(data+hora+' IP:'+ip+'\n')
    arquivo.close() 
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        completion = validate(username, password)
        nomearquivo = 'acessou'+'.txt'
	if completion ==False:
	    error = 'Invalid Credentials. Please try again.'
	    nomearquivodois =	'tentativaacesso'+'.txt'
	    if os.path.exists(nomearquivodois):
                append_write = 'a'
            else:
                append_write = 'w'
            arquivo = open(nomearquivodois,append_write)
            arquivo.write(data+hora+' IP:'+ip+' USUARIO:'+username+' SENHA:'+password+'\n')
            arquivo.close() 	
	else:
            if os.path.exists(nomearquivo):
		append_write = 'a'
	    else:
		append_write = 'w'
	    arquivo = open(nomearquivo,append_write)
	    arquivo.write(data+hora+' IP:'+ip+' USUARIO:'+username+' SENHA:'+password+'\n')
            arquivo.close()						 
    return render_template('login.html', error=error)

def validate(username, password):
    con = sqlite3.connect('static/Usuarios.db')
    completion = False
    with con:
                cur = con.cursor()
                cur.execute("SELECT * FROM Login")
                rows = cur.fetchall()
                for row in rows:
                    dbUser = row[1]
	            dbPass = row[2]
	            if dbUser==username and dbPass==password:
		    	completion=True	

                        
    return completion


if __name__== "__main__":
    app.run('0.0.0.0',80)
